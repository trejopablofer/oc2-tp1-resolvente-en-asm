# OC2 TP1 - Resolvente en ASM

Trabajo Práctico n°1 de Organización del Computador 2

En este documento se describirá la resolución del enunciado del TP1. Éste consistió en el desarrollo de un programa que realice la fórmula resolvente sobre tres valores dados por el usuario. Este proceso se dividió en 3 instancias:
- Realizar un programa en lenguaje ensamblador que calcule las raíces de una función cuadrática. 
- Hacer un programa en C que solicite los valores a, b y c para calcular las raíces.
- Compilar y linkear los archivos objeto de ambos programas y obtener un ejecutable que muestre por consola las raíces obtenidas. En caso que el discriminante b^2-4ac=0 ó a=0 devuelve un mensaje de error.

A continuación se describirá el desarrollo de cada instancia.

## Resolvente en ASM
En primer lugar, se desarrolló un programa en lenguaje assembler capaz de realizar este cálculo sobre valores preseteados en el mismo. Para esto se utilizó SASM, un entorno de desarrollo para el lenguaje NASM (Netwide Assembler), que brinda todas las herramientas de corrección y debug idóneas para este trabajo.

Todas las operaciones que van a ser mostradas a continuación fueron realizadas utilizando la unidad de punto flotante y su set de instrucciones, además del set de instrucciones de NASM.

<p align="center"><img src="sasm.png"/></p>

En este programa primero se declara e inicializa en la sección de datos toda la información que voy a usar, la cual contiene:
- Los mensajes para mostrar los resultados, ya sea las raices o los mensajes de a=0 y de que la fórmula no tiene raíz.
- Las variables "a", "b" y "c", y la "d" en valor 4 que se utiliza en el fragmento "4ac" del código para realizar la multiplicación. 
- Los resultados parciales de cada operación para utilizarlos en los cálculos siguientes.
- Los resultados finales de la fórmula.

<p align="center"><img src="data.png"/></p>

En la sección de código empiezo a desarrollar las operaciones para realizar la fórmula resolvente, pero primero, me aseguro que el valor de "a" no sea 0.

<p align="center"><img src="chequeo.png"/></p>

En caso de a=0, pido que por pantalla se muestre un mensaje de error y se termine el programa.

<p align="center"><img src="a_cero.png"/></p>

Con eso asegurado, empiezo a calcular 4ac y guardo el resultado en res1.

Luego calculo b^2 y lo guardo en res2.

A los dos resultados guardados los resto y guardo el nuevo resultado en res3.

<p align="center"><img src="4ac.png"/></p>

Ya con b^2-4ac resuelto puedo calcular su raíz cuadrada. Sin embargo, antes debo chequear que no sea un número negativo.

<p align="center"><img src="chequeo2.png"/></p>

En caso que res3 sea menor a 0, pido que por pantalla se muestre un mensaje de error y el valor negativo del discriminante, y termino el programa.

<p align="center"><img src="raiz_neg.png"/></p>

Caso contrario, realizo la raíz cuadrada de res3 con la instrucción fsqrt y la guardo en res4.

<p align="center"><img src="raiz.png"/></p>

Continúo calculando 2a y -b, y los guardo en res5 y res 6 respectivamente.

<p align="center"><img src="2a.png"/></p>

Ya tengo todos los resultados parciales para terminar la fórmula resolvente. Ahora solo resta calcular x1 y x2. Para ello primero calculo la suma de res6 y res4, y la divido por el valor de res5. Guardo el resultado en x1.

<p align="center"><img src="x1.png"/></p>

Luego hago lo propio para sacar x2, solo que en vez de sumar res6 y res 4 calculo su resta y luego la divido por res5.

<p align="center"><img src="x2.png"/></p>

Por último, solo me queda mostrar en pantalla las variables x1 y x2, y terminar el programa.

<p align="center"><img src="print.png"/></p>

## Programa en C
En segundo lugar, se escribió un pequeño programa en lenguaje C que declara a la función CMAIN que se encuentra en la sección de código del archivo en assembler. Al declararla le agrego los tres parámetros que el usuario le va a pasar en breve.

Luego de declararla, en la parte main del código declaro las variables "a", "b" y "c" y le pido al usuario que ingrese el valor de cada una, y lo guardo en la variable correspondiente.

Por último uso la función CMAIN y le paso por parámetro las variables a la fórmula resolvente.

<p align="center"><img src="main.png"/></p>

## Linkeo y ejecutable
Lo último para hacer en este trabajo fue el linkeo del código assembler con el programa en C. Para ello primero se trasladó el código del archivo .asm a un archivo .s, ya que éste me permite crear el código objeto del programa.

<p align="center"><img src="archivos.png"/></p>

Cabe aclarar que al archivo .s se le hicieron algunas modificaciones con respecto al .asm. En primer lugar, se le eliminó la macro de NASM %include "io.inc" ya que no sirve para el nuevo formato.

<p align="center"><img src="mod1.png"/></p>

La más importante de las modificaciones fue el agregado de operaciones de pila para que el programa obtenga los parámetros que recibe de C. Estos parámetros los recibe en la pila y para guardarlos en las variables internas de mi programa hubo que desapilarlos de la siguiente forma:

<p align="center"><img src="mod2.png"/></p>

Primero hay que inicializar la pila para que los valores que junte después sean los correctos. Al ser valores float los que entran, para extraerlos de la pila hay que recorrerla de 4 en 4 desde ebp+8, ya que en las primeras dos posiciones de la pila se encuentran datos irrelevantes para este paso que tienen que ver con la función CMAIN.

La última modificación viene atada al uso de la pila. Para que no surja error de violación de core se tiene que cerrar la pila de esta manera:

<p align="center"><img src="mod3.png"/></p>

Ya con el archivo .s listo puedo generar su código objeto. Para ello abro la terminal en su ubicación y escribo el siguiente comando:

<p align="center"><img src="nasm.png"/></p>

Esto me genera el archivo Res.o, el cual voy a necesitar para el linkeo con mi archivo main.c que contiene el programa. El linkeo se realiza con la siguiente línea:

<p align="center"><img src="gcc.png"/></p>

Lo que acaba de hacer este comando es generar un archivo ejecutable que, al correrlo, primero ejecuta el programa en C.

<p align="center"><img src="ce.png"/></p>

Al tener el input del usuario, se lo pasa al programa en assembler, el cual lo recopila y opera en base al mismo.

<p align="center"><img src="c.png"/></p>

En la imagen se pueden apreciar las raíces obtenidas con a=1, b=4 y c=3. Pero en el caso que a sea igual a 0 aparecerá un mensaje distinto.

<p align="center"><img src="a0.png"/></p>

De igual modo, si el discriminante termina siendo un número negativo también se lo hará saber al usuario:

<p align="center"><img src="disc.png"/></p>

Para terminar, tuve que crear un archivo script.sh que corra el ejecutable generado anteriormente.

<p align="center"><img src="sh.png"/></p>

También le agregué mensajes descriptivos acerca del programa que se está ejecutando.

<p align="center"><img src="script.png"/></p>

De esta forma se le da cierre a todo el trabajo realizado.

## Conclusiones
Durante el desarrollo de este trabajo hubo muchas complicaciones que me desvelaron, y casi todas relacionadas con la FPU, empezando con la impresión de los números de punto flotante sacados de la pila FPU por pantalla y terminando con el pasaje de parámetros de los float de C. También tuve algunos problemas en entender los ejercicios obligatorios (sobre todo el 7) y los contratiempos relacionados a las otras materias. Sin embargo pude sobreponerme ante todo eso y presentar un TP a tiempo que considero aceptable de acuerdo a las circunstancias, además de haber aprendido muchas cosas que no sabía.
