extern printf
section .data

msgx1 db "X1 = %f" ,10,0
msgx2 db "X2 = %f" ,10,0

msga0 db "a no puede tener valor 0",10,0
msgnoraiz db "No tiene raiz ya que b^2-4ac = %f",10,0

a dq 1.0
b dq 4.0
c dq 3.0
d dq 4.0

res1 dq 0.0 ;4ac
res2 dq 0.0 ;b2
res3 dq 0.0 ;b2-4ac
res4 dq 0.0 ;/b2-4ac
res5 dq 0.0 ;2a
res6 dq 0.0 ;-b

x1 dq 0.0 ;-b+/b2-4ac:2a
x2 dq 0.0 ;-b-/b2-4ac:2a

section .text
global CMAIN
CMAIN:

    push ebp
    mov ebp, esp

    ;asigno valor a las variables a, b, c
    finit
    fld dword[ebp+8]
    fst qword[a]
    fld dword[ebp+12]
    fst qword[b]
    fld dword[ebp+16]
    fst qword[c]

    ;chequeo a!=0
    mov eax, dword[a+4]
    cmp eax, 0
    je a_cero

    ;4ac
    xor eax, eax
    finit
    fld qword[a]
    fld qword[c]
    fmul
    fld qword[d]
    fmul
    fst qword[res1]
    
    ;b2
    fld qword[b]
    fld qword[b]
    fmul
    fst qword[res2]
    
    ;b2-4ac
    fld qword[res2]
    fld qword[res1]
    fsub
    fst qword[res3]
    
    ;/b2-4ac
    mov eax, dword[res3+4] ;2da mitad del qword me basta para saber si es menor a 0 o no
    cmp eax, 0
    jl raiz_neg ;(jump less, si es negativo no se puede hacer raíz, cambio el flujo)
    fld qword[res3]
    fsqrt
    fst qword[res4]
    
    ;2a
    fld qword[a]
    fld qword[a]
    fadd
    fst qword[res5]
    
    ;-b
    fld qword[b]
    fchs
    fst qword[res6]
    
    ;-b+/b2-4ac:2a
    fld qword[res6]
    fld qword[res4]
    fadd
    fld qword[res5]
    fdiv
    fst qword[x1]
    
    ;-b-/b2-4ac:2a
    finit ;si no limpio el stack, X2 me sale igual a error por la división pasada
    fld qword[res6]
    fld qword[res4]
    fsub
    fld qword[res5]
    fdiv
    fst qword[x2]
    
    ;print
    push dword[x1+4]
    push dword[x1]
    push msgx1
    call printf
    add esp, 12
    
    push dword[x2+4]
    push dword[x2]
    push msgx2
    call printf
    add esp, 12
    
    jmp leave
    
    a_cero:
    push msga0
    call printf
    add esp, 4
    
    jmp leave
    
    raiz_neg:
    push dword[res3+4]
    push dword[res3]
    push msgnoraiz
    call printf
    add esp, 12
    
    jmp leave

leave:	
    mov esp, ebp
    pop ebp
    xor eax, eax
    ret
