%include "io.inc"
extern printf
;Función donde se defina un vector de números float de precisión simple y se calcule la suma
section .data
vector dd 1.0, 2.5, 3.05, 5.005, 8.0005, 13.00005, 21.000005 ; total = 53.555555
resul dq 0.0
msg db "La suma de todos los vectores es: %f" ,0

section .text
global CMAIN
CMAIN:
    push vector
    call suma_vf
    add esp, 4
    push dword[resul+4]
    push dword[resul]
    push msg
    call printf
    add esp, 12
    xor eax, eax
    ret
    
    suma_vf:
    push ebp
    mov ebp, esp
    mov eax,[EBP+8]
    fld dword[eax]
    mov ebx, 1
    mov ecx, 6
    
    ciclo:
    fld dword[eax+4*ebx]
    faddp 
    fst qword[resul]
    inc ebx
    loop ciclo
    mov esp, ebp
    pop ebp
    ret